---
output:
  html_document:
    toc: false
---

<center>
<img src="https://cdn.discordapp.com/attachments/748940478250221578/953690078281105408/bsf.png" width="400"/>
</center>

# BSF beginner routine (PPL)

PPL for beginners is usually fine if they don’t overdo the volume. We’ve all been there that we want to lift 6-7x a week since we want to go all out on our newfound hobby. This template has simply do 5 movements per day, on which you can go all out 6x a week as a beginner. So enjoy! I hope it gets you a lot of training fun and decent gains. I would still recommend a 3x a week program over this one for beginners, but you should gain about as well as you would on PPL as a beginner on this one.

```shell
# LEGS - Squat focus
[Squat] Squat 4x6
[Hip hinge] RDL 3x6
[knee extention] lunges 3x8
superset:
1. [calves] calve raises 3x12
2. [Abs] Planks or any other ab movement 3x

# PUSH - Bench focus
[H press] Bench press 3x6
[V press] OHP 3x6
[H Press 2] Incline DB press 3x8
superset:
1. [Triceps] Tricep ext 3x8
3. [Abs] Planks or any other ab movement 3x

# PULL - Deadlift focus
[Hip hinge] Deadlift 1-3x6 (1 to 3 sets)
[H pull] Dumbell row 3x8
[V pull] chin ups 3x8
superset:
1. [Biceps] curls 3x8
2. [Abs] Planks or any other ab movement 3x

# REST

# REPEAT!
```

## Progression model

As a beginner you should be able to add weight session to session on your lifts. Try to add at between 1-2.5kg on your upper body compounds per session, and between 2.5-5kg on the lower body compounds. Isolations like curls may only add 0.5kg per session.

You should be able to run this program for about 3-6 months of propper training. You will start to feel that you can't recover from the program at some point. And that you can't add weight on your lifts every session. After that you need to switch to an intermediate program. You should feel it coming, workouts feel to heavy, and your lifts should have stopped going up for a few weeks.

## Training advancement 

The table bellow shows how much strength you can realistically gain as a natural strength trainee on a good all around program like this one. It is fairly basic but should give you a good indication of how strong you would be after this program. A better source for strength standards would be for it would be [symmetricstrength.com](https://symmetricstrength.com/), which also factors in weight, age, gender and in far better in general. 

| Lift     | 1RM  (3-6 months)      | 4-6RM (3-6 months)     |
|----------|------------------------|------------------------|
| Squat    | 120-140 kg, 265-300lbs | 100-120 kg, 225-255lbs |
| Bench    | 80-100 kg,  175-220lbs | 70-85 kg,   150-190lbs |
| Deadlift | 160-180 kg, 350-400lbs | 135-150 kg, 300-340lbs |

## Can I change anything about this program?

Run the program as it is written. If you want to change anything about the program you can ask in the server and if you have a valid point we might change it.
We are easy to contact on discord and usually respond very fast ([BSF on discord](https://discord.gg/n6subSZK)). But in general, take the advice from people who have been lifting for a long time. Don't cheat yourself out of your own gains by being a smart ass.

FAQ:

* **Can I add more sets?** Short awnser, no. Adding in more sets is not gonna make you grow better.

* **Can I do an alternative to squats?** Learn how to squat, you can't replace it. It probably the most difficult movement but will give you the best long term progress.

* **Are reps between 8-12 not better to grow muscle?** Not as a beginner. You can't generate enough fatique for lower reps to be ineffective for muscle hypetrophy yet on a program like this one. Lower reps are good for learning form as well. And a beginner program should foccus on learning you the propper form first.

* **Why is it legs push pull and not push pull legs?**
On PPL the pull and leg days usually conflict with eatch other. You can't do deadlifts very well if you plan to do squats the next day. This is an easy fix for that.

## Basic Diet rules
You will probably want to optimize your diet to get the most out of your training. It is recommended to bulk about 0.5 - 1.0kg a month as a beginner. It is NOT recommended to cut as a beginner. The only reason why you would want to cut if you are obese or have fair amount of fat. Beginners usually cut to soon, wanted a 6 pack they don't have yet, and miss out on a lot of progress. Finally maingaining is a meme, you can't build a lot of muscle if you don't eat enough. The first plateau's you will encounter can be solved by just eating more. Do keep in mind that if you gain bodyweight, your lifts HAVE to go up. Or else you'll get fat.

## About the chin ups.

Chin ups can be done with bands to make them easier. As you get to a desired 8 reps on they you will want to remove the bands, and eventually add weight to them.

## Autoregulation

To explain autoregulation very simple: it is a system in a program that dictates if you should do more or less sets, reps or intensity.
For the deadlifts, feel free to do between 1 and 3 sets. For some people 3 might be too much, and others might rep it out without any problems.
