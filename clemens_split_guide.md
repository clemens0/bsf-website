---
title: "BSF training guide"
output:
  html_document:
    toc: true
---

<style>
table, th, td {
  text-align: left;
}
</style>

# Introduction

> This guide is made to help you create your own training splits for the most common training splits on bodybuilding. It might look lengthy, but that's because there are so many programs listed here. A lot of beginners want to create their own training program, or at least have a say in which movements to do, how many times a week they want to train etc. This guide exists to give those individuals the freedom to make and change their own routine. This guide should help you to prevent to create something that might not work, or might be too far from optimal to see significant progress. I believe in learning by example, so there will be a fuck ton of programs in this webpage. Good luck! ~ Clemens.

## Training advancement and frequency

The first think you might want to ask yourself when you're looking for a training program is what kind of schedule do you have. If you're able to only lift 2 or 3 times a week, a *full body* routine might be more benificial for you than a *push pull legs* split would be.
If you're a teen who wants to spend the most amount of time he can at the gym a PPL split might be better. It depends a lot on the individual. This is the first thing you want to keep in mind when choosing a training split

Another important thing about frequency is that your program fits your level of advancement. A beginner on a beginner program will gain size and strength maybe 3x faster than on for example the Texas Method, Madcows, or a crazy PPL split with 20+ sets per day. This section describes what frequency/split type would be most optimal for muscle growth and strength gain. But if you simply don't like the type of training I'm discussing here you can skip ahead to the next session. You can use one of the standaard templates for beginners for UL or PPL etc instead. This guide might still be usefull if you want to bring up specific bodyparts that are still in the nooby stage of training.

Beginners can usually benefit from more frequency per muscle group. If you're new to for example squats, you will recover every 48 hours usually. You can't generate enough force yet to create a lot of fatique and muscle damage. If you're still trying to foccus on the technique of squats you might even want to train the squat every 24 hours as a beginner for a few weeks, untill you feel that you need the 48 hour recovery time.
The following squat progression follows a lifter starting with an empty bar, who is learning how to squat (we use squats as an example but can use this principle for most compounds).
As you can see table follows the progression an 2.5kg increment per lifting session.
The moment the lifter feels underrecovered, too fatiqued, or misses a new PR, they should switch to a 3x a week sceme.
Do keep in mind that the weight on the lifts will differ from individual to individual.
It is also recommended to do any stretches needed by the lifter to preform the basic compounds at this point if they are not flexiable enough.

```
Week 1

DAY       LIFT        STATUS
Monday    Squats 20kg Stimulus
Tuesday   Squats 22kg Growth
Wednesday Squats 25kg Growth
Thursday  Squats 27kg Growth
Friday    Squats 30kg Growth

Week 2

DAY       LIFT        STATUS
Monday    Squats 32kg Growth
Tuesday   Squats 35kg Growth
Wednesday Squats 37kg Fatique but growth
Thursday  Squats 40kg Fatique but growth
Friday    Squats 40kg Not recovered

Week 3

DAY       LIFT        STATUS
Monday    Squats 42kg Growth
Wednesday Squats 45kg Growth
Friday    Squats 47kg Growth
```

As you become an intermediate the rate at which you make progress slows down again. For most people this can be solves by just eating more. But to keep this paragraph just about programming we'll talk about what you should do next.
In the example bellow the novice lifter stalls at 102kg. He feels fatiqued and is not able to progress from session to session.
This fatique has usually been building up for multiple weeks. In most cases a deload is in order.
We can simply lower the frequency of the amount of heavy session we do, by making wednesday a lighter day, or taking 2 days rest instead of 1.
The first solution allows to still run a full body program 3x a week, and the 2th solution might look more like an upper lower split.
At this point the lifter would be an starting intermediate.
Also at this point there will be a more distinct line between strength training/powerlifting and hypetrophy.
```

LIFTER STALLS

DAY       LIFT           STATUS      
Monday    Squats 102.5kg Stimulus
Wednesday Squats 102.5kg Under recovered
Friday    Squats 102.5kg Under recovered

SOLUTION 1, light day (Usual strength program)

DAY       LIFT           STATUS      
Monday    Squats 102.5kg Stimulus
Wednesday Squats 80kg    Stimulus
Friday    Squats 105kg   Growth

SOLUTION 2, lower frequency  (Usual hypetrophy program)

DAY       LIFT           STATUS      
Monday    Squats 102.5kg Stimulus
Thursday  Squats 105kg   Growth
```

Also at this point there will be a more distinct line between strength training/powerlifting and hypetrophy.
For hypetrophy after this stage not much changes after this stage of periodisation. You will train most of your muscles 2x a week to let them recover.
You might need to deload again if you feel fatiqued from months of lifter.
For strength the periodisation becomes a bit more complicated. I would highly recommend reading *Practical program by Mark Rippetoe* if you're interested in this topic.

## Exercise selection

Lets talk about excercise selection. Bellow you can find all the points I deem important for picking an excersice.

First thing's first, just MUST work your entire body. You don't want to end up looking unproportional because you only train your biceps and arms. If you're training for bodybuilding you will want to have at least  the following movements in your program. After the movement I've provided some examples of what excersices they represent. We can make exercise selection a lot more difficult, but if you follow the table bellow you're at least at the 80% mark of what would be optimal.

| Compound movements | Examples |
|----------- |----------------------------------------------------------------------------|
| Hip hinge  |Deadlift, RDL |
| Knee flexion / squat | squat, hack squat, leg press |
| Horizontal press | bench press, dumbell press, push ups, dips)| 
| Vertical press |barbell OHP, dumbell OHP)|
| Horizontal pull |Pendleys rows, Bodybuilder barbell rows, dumbell rows, seal rows |
| Vertical pull | pull ups, chin ups |

| Recommended isolations | Examples |
|----------- |----------------------------------------------------------------------------|
| Biceps | Ez bar curls, DB curls, spider curls |
| Triceps | Ez bar overhead extentions, skullcrushers, tricep pushdowns |
| Calves |Standing calve raises, calve machine |
| Abs | incline crunches, leg raises, crunch machine |
| Lateral delt isolation | DB Lateral raises, cable lateral raises, lateral raise machines |
| Rear delt isolation | reverse pec dec, face pulls |
| Forearm training | reverse curls, hammer curls, wrist curls |
| Neck training | Neck extentions, neck curls |

- Another point I want to make is that you HAVE to train the compounds. You don't have to do the powerlifting movements but especially as a beginner there should be no reason not too. They might be fairly difficult to learn. Squats for example are difficult if you're not mobile enough, you lose balance quickly and the are scary. But in the end the big movements are going to get the biggest. The compounds work multiple muscle groups, unlike for example preacher curls or a crunch. They safe you a lot of time and especially as a beginner you may need less isolations.

- It doesn't really matter if you use machines or free weights in my oppinion. Preferably you will want to learn the compounds as a beginner since these movements are the most resilient.

- I want to make another point about at home workouts. They are simply not optimal. Push up variations are no replacement for a barbell and dumbells if you want the best results possible. That being said, callistanics and at home dumbell training are perfect to get you started with your fitness journey.

## Sets and Reps

Last up we need to talk about sets and reps. It has become pretty lengthy so you can read about how to create sets, which rep ranges to use etc on [Sets and Reps](https://clemens0.gitlab.io/bsf-website/sets_and_reps.html)

# Training splits

## Full body

### Advantages vs disadvantages

Lets start with full body training. Its usually a perfect good pick for beginners for hypetrophy training, but can also be used for advanced trainee's if you know how to use it.

Advantages of the full body split:

* It's very simple, you just train your full body, usually every other day or 3 days a week.

* It's very scalable, you can lift anywhere from 2 times a week to 5 times a week in special training splits.

* It's very high frequency: if you're a beginner you can overload your lifts 3-4x a week, which is twice as much as on a UL or PPL split.

Disadvantages of full body splits

* Sessions can become very long, and might take more than 2 hours if you plan to do it 2-3 times a week. If you're more advanced you need more volume for hypetrophy and session will have maybe 20+ sets.

* Sessions are very hard. It's by far one of the hardest splits, especially as a more advanced trainee if you use a basic full body 3x a week template.

* Frequency will be too fast if you're an intermediate/advanced lifter if you train heavy too frequency. Training a bodypart every 48 might leave you under recovered.

### Beginner vs Intermediate

A beginner can usually make progress every lifting session spread appart 48 from eatch other.
The problem arises when a novice lifter becomes an intermediate.
The fatique and muscle damange will not have recovered after 48 hours and the trainee will not be able to lift heavy 3x a week anymore.
At this stage the lifter has to make either one of two choices:
1. The trainee switches too a Upper lower, PPL, or PP split. Which allow for lower frequency.
2. The trainee switches up the days between intense and less intense days. By for example not doing heavy compounds on the wednesday sessions, and only training arms on this day with light isolation / reduces load compound work.

### Beginner programs

#### SL / SS variation 

A more balanced Stronglifts 5x5 / Starting strength. Your DL will have more foccus on this variation.
It also has more arm isolation so you won't look like a facking turtle at the end of the program.
This variation of the program also actually allows you to get your abs, unlike SS and SL.
I also don't like that they let you do rows for sets of 5, I think this is too low and most beginners will cheat it.
In general I'm not a huge fan of rows for beginners, so feel free to replace them with pull ups.

```
A rest B rest repeat (3-4x a week)

WORKOUT A:
Squats 3x5
Bench 5x5
Rows 3x8 (BB row, pull ups even... your pick)
superset:
1. EZ curls 3x8-12
2. EZ overhead extentions 3x8-12
3. Planks or any ab movement

WORKOUT B:
DL 1x5
OHP 5x5
Squats 3x5
Superset:
1. Reversed curls 3x8-12
2. Tricep pushdowns 3x10-15
3. Planks or any ab movement
```

#### 3 day rotation

If you don't like the example above you may like to rotate movements every 3 days and have a full weekly program.
In this program we start with a hip hinge day on monday, on wednesday we do a horizontal press foccused program and on the friday we do prioritize a back movement.

```
Monday:
[main Knee hinge] Squats 3x5
[Vertical press] OHP 5x5
[Horizontal pull] Barbell rows 3x8
[biceps] 3x8 ez bar curls
[triceps] 3x8 ez bar overhead extentions

Wednesday:
[Horizontal press] Bench press 5x5
[Vertical pull] chin ups 3x8
[knee hinge] front squat 3x8
[abs] 3x30 seconds weighted planks
[calves] 3x15 standing calve raises

Friday:
[Main hip hinge] DL 1x5
[Horizontal press] Dips 3x5
[Vertical press] OHP 3x8
[knee hinge] Squats 3x5
[biceps] 3x8 ez bar curls
[Horizontal pull] pull ups 3x8
```

#### Full body with extra arm day

In the program bellow you can see that we have 4 training days. The important thing about this program is the arm day on the saterday.
One of the big benefits from full body is that we can train the full body withing 24 hours, given that we emphesise different muscles on full body days.
In this program we emphese the thorso on friday, whole we emphesise the arms on a saterday. The friday session focusses on shoulders, quads, upper back while the saterday session focusses on chest lats and arms. There are still overlapping muscle, namely the ticeps, chest and biceps to and extend.

```
Monday:
Bench 3x6
Squat 3x6
wide barbell Rows 3x8
lateral raises 3x12

Wednesday:
Deadlifts 1x5
Bench 3x6
Pull ups 3x8
calve raises 3x12

Friday:
Squat 3x6
OHP 3x6
wide grip rows 3x8
lateral raises 3x12

Saterday:
dips 3x8
chin ups 3x8
bicep curls 3x10
skullcrushers 3x10
```

### Intermediate programs

#### Texas method

The texas method is more of a template than a cookie cutter program.
So it is recommended to read Practical programming from Mark Rippetoe before running the program.
Also Justin Lescek - The Texas Method part 1 and 2 are very good reads if you want start training this way.
In short this method has you train at 3 different intensities 3x a week to manage your fatique as an intermediate lifter.
The idea is that you use the volume day to drive your intensity day in friday, while the wednesday session help you add some extra volume in there, practice the lifts' forms and get some blood in your muscles.
So this method uses undulating periodization. But you could even do top and back of sets if you wanted monday and friday's sessions to look more simulair.
Or you could use ramped up sets like Madcow 5x5. This also allows for some extra volume to drive the top sets.
The program bellow is a program made by the template of the texas method and should give you an idea what it looks like.

The percentages differ a lot from trainee to trainee. They are simply just general guidelines. An easy rule to follow is that your volume days and your inensity days should feel about the same on how hard they are to preform. If your volume days feel super heavy, and your intensity days feel very light, you need to lower the weight / volume on the volume day.

```
MONDAY - Volume day
5x5 Squats @85 of 5RM
5x5 OHP/Bench (alternating) @85 of 5RM
3x8 Barbell Rows
3x10 Tricep overhead extentions
3x10 EZ bar curls

WEDNESDAY
2x5 Squats @85 of monday's session
3x5 Bench/OHP (alternating) @85 of last monday's session
3x8 hyperextentions
3x10 Tricep pushdowns
3x10 Reverse curls

FRIDAY - Intensity day
1x5 Squats @ 5rm
1x5 OHP/Bench (alternating) @ 5RM
1x5 DL @ 5rm
3x10 Tricep overhead extentions
3x10 EZ bar curls
```

#### Texas method, foccus on your weak points

At some point you may need to be more specific on your weak points and just foccusing on the main lifts is not enough anymore.
If you find yourself always failing at the bottom of your bench press, and you're not strong enough off the chest, you may want to try paused bench presses on your volume day to specifically work on your weak point. This will translate over to the competition lift on friday. But if you are weak at lets say the top part of your lift, you may want to add chains or bands to your bench. You can even try a closer grip variation. Bellow you can find two different variations for the volume days that describes volume days for a lifter that is weak af all the top and bottom parts of the compeititon lifts. You may want to decide for yourself which lifts you have which specific weak points first if you want to try this technique. I also didn't add any arm and ab accessory works since I assume you know which accessories you will want for these as an intermediate and where you will want to place them.

I also made this program so it specifically works the powerlifting competition movements. The OHP is not a main movement in this program. I also added front squats on the wednesday session, just for the variation of the programs I list here since this is also a valid option.
```
MONDAY - Volume day (weak at the bottom of the lifts)
5x5 Paused Squats @80 of 5RM
5x5 Paused Bench @80 of 5RM
3x5 Deficit Deadlifts / paused deadlifts @ RPE 6-7

MONDAY - Volume day (Weak at the top of the lifts)
5x5 Paused Squats with bands/chains @85 of 5RM
5x5 Bench with bands/chains @85 of 5RM
3x5 Deadlifts with bands/chains @ RPE 6-8

WEDNESDAY
2x5 front squats @ RPE 8
3x5 OHP @ RPE 8
3x8 hyperextentions

FRIDAY - Intensity day
1x5 Squats @ RPE 8
1x5 Bench @ RPE 8
1x5 DL @ RPE 8
```

#### Ramped up sets

Ramped up sets are basically very heavy warm up sets. Its just another method to manipulate intensity, since we want to train in multiple intensities as an intermediate lifter. The program bellow serves as an exmple of how a program that uses this method looks like.
On the monday we work up to a top set of 100% of your 5RM. We first do warm up sets of 50 to 85% to get in extra volume we need.

```
Monday:
Squats 1x5 - Ramped up @% 50, 60, 75, 85, 100
Bench 1x5 - Ramped up @% 50, 60, 75, 85, 100
Deadlifts 1x5 - Ramped up @% 50, 60, 75, 85, 100

Wednesday:
Squats 2x5 @ 75% of 5RM
OHP 3x5 @ 75% of 5RM
Back extentions 4x10 

Friday:
Squats 1x5 - Ramped up @% 50, 60, 75, 85, 100
Incline bench 1x5 - Ramped up @% 50, 60, 75, 85, 100
Rows 1x5 - Ramped up @% 50, 60, 75, 85, 100
Close grip bench 3x8 @ RPE 8
Chin ups 3x8 @ RPE 8
```

#### Top sets and back off sets

We can also use top and back off sets in the same day, instead of speading them out like with the Texas Method.
In the program bellow you can see an example of this.

```
Monday - competiton lifts
1x5 Squats @ RPE 8
2x5 Squats @80 of 5RM
1x5 Bench @ RPE 8
2x5 Bench @80 of 5RM
1x5 DL @ RPE 8

Wednesday - light essesory work
2x5 front squats @ RPE 8
3x5 OHP @ RPE 8
3x8 hyperextentions

Friday - Weak points day
1x5 Paused Squats @ RPE 8
2x5 Paused Squats @80 of 5RM
1x5 Paused Bench @ RPE 8
2x5 Paused Bench @80 of 5RM
1x5 SLDL @ RPE 6-7
```

#### Fatique percentages

Bellow you can find an intermediate program designed with fatique percentages. Be sure to check out the informative section of the webpage first if you don't know what fatique percentages are.

```
Pause squats 5% drop load 4-6 reps @ RPE8
Pause bench 5% drop load 4-6 reps @ RPE8
Barbell rows 5% drop load 4-6 reps @ RPE8
Closegrip bench 5% drop load 4-6 reps @ RPE8

Front squats 5% repeat 8 reps @ RPE8
incline bench 5% repeat 8 reps @ RPE8
back extentions 5% repeat 10 reps @ RPE8
bicep curls 5% repeat 10 reps @ RPE8

Squats 3% drop load 4-6 reps @ RPE8
Bench 5% drop load 4-6 reps @ RPE8
Deadlifts 3% drop load 4-6 reps @ RPE8
Closegrip bench 5% drop load 4-6 reps @ RPE8
```

## Upper Lower

### Advantages vs disadvantages

Advantages:

* Its decently scalable, since we have a lot of rest days thoughout the week. We can simply move rest/session days if something comes up. You can also run the Upper lower program with 2 and 3 times a week frequency.

* It is fairly simple, you simply train all your upper body muscle and the other session  you train your legs.

* It is a very good 4x a week template, which allows for moderate volume per session and fairly short sessions unlike full body.

Disadvantages:

* Movements like Deadlifts are might be more difficult to place, since it overlaps on both upper and lower body.

* The lower back overlaps between a lot of movements on upper and lower sessions, like OHP Squats Deadlifts. This might not allow it to recover properly, especially on heavier movements.

* It allows for a lot of volume on the lower body, while usually lifters need more volume on upper body.

### Example 1: vanilla upper lower

the deadlifts might interfere with the rest of the training days so we put it in the 6th day

```
Day1: Upper A
Day2: Lower A
Day3: 
Day4: Upper B
Day5:
Day6: Lower B
Day7:
```

```
UPPER A
V Pull       - BB rows
H Pull       - pull ups
V Push       - Bench
H Push       - OHP
lateral delt - lateral raises
triceps      - skullcrushers
biceps       - EZ bar curls

LOWER A
Squat        - High bar Squat
2th DL       - RDL
quads        - Leg extentions
hamstrings   - leg curls
calves       - calve raises

UPPER B
V Push       - Bench
H Push       - OHP
V Pull       - lat pulldowns
H Pull       - DB rows
lateral delt - lateral raises
triceps      - Overhead extentions
biceps       - DB curls

LOWER B
DL           - DL
2th squat    - Front squat
hamstrings   - Hamstring curls
quads        - Leg curls
calves       - calve raises
```

### Example 2: PHUL

TODO

## Push Pull

### Advantages vs disadvantages

Advantages:

* The push and pull days are very balanced. Both days have simulair muscle groups, except they are on the back and front of your body. It is easy to give your full body equal volume spread on the push and pull days.

* unline UL and PPL we can simply do deadlifts on one day, and squats on the other.

* Like upper lower, its decently scalable, since we have a lot of rest days thoughout the week. We can simply move rest/session days if something comes up. You can also run the template with 2 and 3 times a week frequency.

* It is a very good 4x a week template, which allows for moderate volume per session and fairly short sessions unlike full body.

Disadvantages:

* Push and Pull movements might be difficult to distinguish by beginners. It is not as intuitive as UL.

* The lower back, glutes, hamstrings and quads overlap between a lot of movements on push and pull sessions, like Squats and Deadlifts. This might not allow it to recover properly, especially on heavier movements.

### Example 1: Basic Push Pull

```
Day1: Push A
Day2: 
Day3: Pull A
Day4: 
Day5: Push B
Day6: Pull B
Day7:
```

```
PUSH A
Squats 3x5
OHP 3x5
Bench Press 3x5
Skull crushers 4x8-12
Calf raises 3x8
Crunches 3x8

PULL A
Deadlifts 1x5 
Barbell rows 3x5 
Pull ups 3x8 
EZ bar curls 4x8-12

PUSH B
Bench Press 3x5
OHP 3x5
Squats 3x5
Rope pushdowns 4x10-15
Planks 3x8
Calf raises 3x8

PULL B
Pull ups 3x5
RDL 3x5 
Barbell rows 3x5 
DB curls 4x8-12

```

## Push Pull Legs

### Advantages vs disatvantages

Advantages:

* PPL allows for very short training session, since you train so frequently.

* It allows for a lot of volume per session since training session take so little time.

* It's fun, you get train to train 6x a week.

* The PPL template lasts for a long time, you can use the same basic setup for beginners and advanced trainee's, given that the volume is lower for beginners.

Disatvantages:

* PPL is not very scalable, you can't train your arms 3x a week on a standaard template if you would want too for example. You can't miss a single sessions or you can't make PPL work since there is no room to move a session a day ahead.

* The original PPL template doesn't give you a rest day between the Pull and Leg sessions, which have major overlapping musclegroups like the lower back, hamstrings etc. It is usually recommended to run PPL as Push Pull rest Legs. This variation allows for better recovery.

* It might be difficult to place movements like deadlifts, since they have overlapping muscle groups. 

### Example 1: Beginner PPL

PPL for beginners is usually fine if they don't overdo the volume. We've all been there that we want to lift 6-7x a week since we want to go all out on our newfound hobby. This template has simply do 5 movements per day, on which you can go all out 6x a week as a beginner. So enjoy! I hope it gets you a lot of training fun and decent gains.
I would still recommend a 3x a week program over this one for beginners, but you should gain about as well as you would on PPL as a beginner on this one.

```
PUSH
[H press] Bench press 3x5
[V press] OHP 3x5
[H Press 2] Incline DB press 3x8
[V press 2] Lateral raises 3x12
superset:
1. [Triceps] Tricep ext 3x8
2. [Abs] Planks or any other ab movement 3x

PULL
[Hip hinge] Deadlift 1x5
[H pull] Barbell row 3x8
[V pull] Pull up 3x8
[H pull] Reverse pec dec 3x12
superset:
1. [Biceps] curls 3x8
2. [Abs] Planks or any other ab movement 3x

REST

LEGS
[Squat] Squat 4x5
[Hip hinge] RDL 3x5
[knee extention] lunges 3x8
superset:
1. [calves] calve raises 3x12
2. [abs] Planks or any other ab movement 3x
```

### Example 2: Homegym PPL

To put it bluntly, a home gym is far from optimal compaired to a gym to grow muscle. Lots of movements feel "hacky" and are difficult to overload with good and small increments. Nevertheless the program should get you some decent gains and some compounds / isolations in this program are actually super good.
The following program assumes you have some basic gym equipment like dumbells, a pull ups bar, and some ropes for a (DIW) RTX set.
If the pulls ups / chin ups are too difficult you can do negative pull ups or just a resistance band to make it easier. If you can get 9 pull ups start using weights to make it more difficult.
The one other movement that has some issues with this program is the dumbell RDL. Most home gym dumbells don't go heavy enough to train your hamstrings with this exercise enough. You may even need to go 20+ reps if you max out your dumbells on this movement. The same holds true with the split squats, these might become too easy after a while.
Another movement that might look new to you is the RTX assisted pistol squat. This movement lets you do a pistol squat while assisting yourself with bands above you to make the squats easier. Over time the movement should get easy to preform and hopefully you can do normal pistol squats after training for a while.

```
PUSH
[H Push] Weighted push up 3x6-9
[V Push] Seated DB OHP 3x6-9
[H Push 2] Floor DB fly's 3x10-13
[V Push 2] Seated Lateral raises 3x10-13
[Triceps] Tricep ext 3-5x8-12
[Abs] Planks 3-6x 30 seconds

PULL
[V Pull] Pull ups 3x6-9
[H Pull] RTX Inverted rows 3x6-9
[V pull 2] Chin ups 3x6-9
[H Pull 2] reverse fly's 3x10-13
[biceps] DB curls 3-5x8-12

LEGS
[squat] Split squats 3x6-9
[hip hinge] Dumbell RDL 4x8-12
[squat 2] RTX assisted Pistol squats 3x6-9
[calves] calve raises 3x10-20
[abs] Planks 3-6x 30 seconds
```

### Example 3: PPL with rotations

```
Day1: Push A
Day2: Pull A
Day3: Legs A
Day4: Push B
Day5: Pull B
Day6: Legs B
Day7: ...
```

```
PUSH
Bench press 3x5-8
OHP 3x5-8
Incline DB press 3x8-12
superset:
1. Tricep ext 3x8-12
2. Planks or any other ab movement 3x

PULL
Barbell row 3x8
Pull up 3x8-12
DB rows 3x8-12
superset:
1. curls 3x8-12
2. Planks or any other ab movement 3x

LEGS
Squat 4x5-8
RDL 3x5-8
Lunges 3x5-8
superset:
1. calve raises 3x10-20
2. Planks or any other ab movement 3x

PUSH
OHP 3x5-8
Bench press 3x5-8
Incline DB press 3x8-12
superset:
1. Tricep ext 3x8-12
2. Planks or any other ab movement 3x

PULL
Pull up 3x8-12
Seal row 3x8 (or any row that spares your lower back)
DB rows 3x8-12
superset:
1. curls 3x8-12
2. Planks or any other ab movement 3x

LEGS
Deadlift 1x5-8
Front Squat 4x5-8
hamstring curls 3x5-8
superset:
1. calve raises 3x10-20
2. Planks or any other ab movement 3x

```

### Example 4: Turning PPL into a bodypart split

Lets say you want to give your arms more frequency than 2x a week you can turn the PPL template into a bodypart split like in the table bellow.
Here we added bicep work on leg day, since they should already be recovered by then, depening on the volume.
We also do the same for triceps and the rest day.
```
Day1: Push + calves
Day2: Pull 
Day3: ...  + triceps
Day4: Legs + biceps
```

On push day you usually do a horizontal press and a vertical press.
Usually the one of the two you do last suffers preformance from the first one.
A solution for this is to add a shoulder day on the rest day where you do a few simple OHP and lateral raise movements.

```
Day1: Push
Day2: Pull
Day3: ...  + Shoulders
Day4: Legs
```

I hope these two examples give you enough information to experiment with the concept of turning the PPL into a bodypart split for yourself.
Do keep in mind that you might not have any rest days left on a split like this one. For some people that might be good since they prefer training 6-7x a week. But keep in mind that your program will become very unflexiable this way, and that you might not fully recover since you don't have any rest days.

TODO split example

## Bodypart splits

TODO PHAT
TODO exel sheets of mesocycles

# Archived programs

The programs found here I have made in the past, or found in the past and modified. I don't think these programs are good programs to run. I do think however that there are smart tricks used in these programs that you can still learn from. Because of that reason I have them listed here.

## Full body PPL

This routine simply switches between two pulling pushing and leg movements every session.
This way eatch part of the PPL rotations gets equal attention.
You could make it even more balanced by rotation the movement sets.
The close grip bench presses and chin ups should suffise as a beginner for arm work for beginners.
As an intermediate lifter you might want to add an extra arm day or extra arm work,
or switch to an intermediate program in general.

```
WORKOUT MONDAY: Push -> Pull --> legs
- Push     - Bench press 3x5
- Push     - Militairy press 3x5
- Pull     - BB or dumbbell row 3x6-12
- Pull     - Chin ups 3x8-15
- Legs     - BB Squats 3x6-12
- Legs     - Seated hamstring curl 3x8-15

WORKOUT WEDNESDAY: Pull--> Legs--> Push
- Pull     - BB or dumbbell row 3x5
- Pull     - Chin ups 3x5
- Legs     - BB Squats 3x6-12
- Legs     - Seated hamstring curl 3x8-15
- Push     - Close grip Bench press 3x6-12
- Push     - Militairy press 3x6-12

WORKOUT FRIDAY: Legs --> Push --> Pull
- Legs     - BB Squats 3x5
- Legs     - RDL 3x5
- Push     - Close grip Bench press 3x6-12
- Push     - Militairy press 3x6-12
- Pull     - BB or dumbbell row 3x6-12
- Pull     - Chin ups 3x8-15
```

## Example 2: Full body PPL superset

Next up it is pretty easy to superset the program we have just made. We can create two giant sets with both a push pull and leg movement.
This allows us to narrow the sessions down to 6 big sets in total which can save a big amount of time. We do take some rest between the excersices and giant sets, but it doesn't have to be a 4-5 minute rest between sets anymore. The rest time will instead be the time it will take to change to plates (or close to 1 to 3 minutes). If your cardio is up to it, and you are short on this this is a great template for a program to run. Supersetting can take away slightly from your preformance, but it should be neglectable giving that you don't have the conditioning of a potato.
This program may especially work well for someone who comes from a sport background like spinting or Crossfit.

I have also added an optinal superset if you feel that you have more in the tank after the compounds. Here you are free to train your abs, calves and arms with isolation movements.

```
WORKOUT MONDAY: Push -> Pull --> legs
Supetset:
- Push     - Bench press 3x5
- Pull     - BB or dumbbell row 3x6-12
- Legs     - BB Squats 3x6-12
Superset:
- Push     - Militairy press 3x5
- Pull     - Chin ups 3x8-15
- Legs     - Seated hamstring curl 3x8-15
Superset (optional):
- triceps  - tricep extention 3x8-12
- biceps   - EZ bar curls 3x8-12

WORKOUT WEDNESDAY: Pull--> Legs--> Push
Superset:
- Pull     - BB or dumbbell row 3x5
- Legs     - BB Squats 3x6-12
- Push     - Close grip Bench press 3x6-12
Superset:
- Pull     - Chin ups 3x5
- Legs     - Seated hamstring curl 3x8-15
- Push     - Militairy press 3x6-12
Superset (optional):
- abs      - weighted planks 3x30 seconds
- calves   - calve raises 3x15

WORKOUT FRIDAY: Legs --> Push --> Pull
Superset:
- Legs     - BB Squats 3x5
- Push     - Close grip Bench press 3x6-12
- Pull     - BB or dumbbell row 3x6-12
Seperset:
- Legs     - RDL 3x5
- Push     - Militairy press 3x6-12
- Pull     - Chin ups 3x8-15
Superset (optional):
- triceps  - tricep extention 3x8-12
- biceps   - EZ bar curls 3x8-12

```
