---
title: "Plateau's"
output:
  html_document:
    toc: true
---

You have plateau'd when a certain lift is not going up anymore. When you've hit a plateau you will probably not gain any more strength or muscle size.
A plateau can be caused by a lot of things. I recommend going over this list step by step to see if there is something you can do to get over your plateau.

1. The first thing to do when you think you have a plateau is to check if you've actually plateaud. Did your your lift go up the last few weeks? Than its not a plateau, its just regulair old slow progress which is normal. The graph bellow might help you with what advancement level you are, which by how long you've been lifting and how many times you can PR in a certain time. If you run a beginner / notive program and you only make a new PR every 2 weeks you might be better off with an intermediate program which takes that into account. If you have been lifting for 2 months and you only make a new PR every month you know you're doing something wrong. Of course this graph is arbritrairy and should only be used as an indicator. 

```
Level              New PR           Years lifted
——————————————————————————————————————————————————
Novice             PR 3 / a week    <3 months
Late novice        PR 2 / week      6 months
Intermediate       PR 1 / week      1 year
Late Intermediate  PR 2-3 / month   1.5 years
Advanced           PR 1 / month +   2+ years
```

2. Eat enough food. If you're cutting big change you're not gonna grow any new muscle. Same holds true to "maingaining". If you don't eat enough food you will simply not grow.
3. Sleep and stress! If you don't sleep about 8 hours a night, or if you feel tired all the time it can explain why you are not breaking though plateau's. The same holds true with stress. If you have an important test comming up prior to your deadlift PR, there is a chance you're not gonna break it.
4. Are you eating enough protein? You can get away with a moderate amount of protein 1.5 x your bodyweight (kg) or 0.7lbs per pound of bodyweight should be the cutoff point which you should not be crossing if you want optimal muscle growth. If you want to know what optimal is for gaining muscle check out the [nutrition guide](https://clemens0.gitlab.io/bsf-website/leight_nutrition_guide.html).
5. Are you training hard enough? Most beginners simply don't train hard enough. Not enough intensity per set, or skipping sets all together. If you're stuck you may want to try to go closer to failure. As an intermediate you might be stuck for a different reason. Going all out gives you a very high stimulation, but it is also very fatiguing. You might be better off with an RPE of 8 for example. This gives you more room to recover and you might actually be able to add another set which can help you grow.
6. This next point is more for the prevention of plateau's instead of getting out of one. You have to train in the full range of motion of the lift. Don't ego lift by doing partial reppetitions. If for example your bench press doesn't touch at your chest, or you bounce it up you may be cutting yourself short of more quallity volume. You can also end up shortening the ROM more an more which actually reduces the effectiveness of the rep. You might add weight to the bar and plateau without even knowing you are actually plateau'd.
7. Is it time for a deload? Usually after lifting moderatly heavy for a month or 2-3 might leave you in a fatiqued and under recovered state. Especially if all your lifts have stalled its a good indicator you might need a deload.
8. Next up you will have to look at your program. Do you get enough volume on your muscle? Or are you by change under recovered? You can look at the volume charts to check if you might be off by a large degree on the lift that is stalled. Also if you never feel sore, or always feel sore it is a good indicator that you are under/overtraining your muscle.

<center>
<img src="https://cdn.discordapp.com/attachments/959015176810688522/960468654221320202/Screenshot_20220404-111904.jpg" width="600"/>
</center>

<center>
<img src="https://cdn.discordapp.com/attachments/959015176810688522/960468654451986432/Screenshot_20220404-111843.jpg" width="600"/>
</center>

<center>
<img src="https://cdn.discordapp.com/attachments/959015176810688522/960468654657515562/Screenshot_20220404-111836.jpg" width="600"/>
</center>


9. Are you training in the right rep ranges? Usually it helps intermediate lifters to lift in multiple rep ranges or intensities to break though plateau's. You might want to incorporate undulating periodization or top and back off sets. Even a very light session where you lift 70-80% of your working set might help with form and help you get though a plateau.
10. You can train the point of the lift you're weakest at. Say you are weak at the bottom of your squats, bench, OHP, DL you can pause those movements for 1-2 seconds at the bottom of the movement for the back off sets. If you're weak at the top you can try to add bands to the volume work on your lifts.
11. Consider doing a strength phase if you're stuck on hypetrophy and visa versa. Gaining more strength will build more muscle for bodybuilding. For powerlifting/strength sports bigger muscles will usually result in more strength.
12. If you're still lost at this point you need to reevaluate the steps above again. Or visit a doctor because something else is wrong.
